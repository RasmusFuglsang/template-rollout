package dk.lockfuglsang.jira.plugins.template;

import dk.lockfuglsang.jira.plugins.template.template.TemplateReplacer;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class TemplateReplacerTest {

  @Test
  public void testGetTemplateKeys() throws Exception {
    // Arrange
    TemplateReplacer test = new TemplateReplacer("%", "%");
    Set<String> expected = new HashSet<>(Arrays.asList("world", "you"));

    assertThat(test.getTemplateKeys("Hello %world%. How are %you% doing?"), is(expected));
  }

  @Test
  public void testGetTemplateKeysEscaping() throws Exception {
    // Arrange
    TemplateReplacer test = new TemplateReplacer("${", "}");
    Set<String> expected = new HashSet<>(Arrays.asList("world", "snack"));

    assertThat(test.getTemplateKeys("My ${world}. For a ${snack}, ${in valid}, ${æøålsoinvalid}"), is(expected));
  }

  @Test
  public void testReplaceWithValues() throws Exception {
    TemplateReplacer test = new TemplateReplacer("%", "%");
    String expected = "Hello darling. How are you feeling and doing?";
    Map<String,String> map = new HashMap<>();
    map.put("world", "darling");
    map.put("you", "you feeling and");

    assertThat(test.replaceWithValues("Hello %world%. How are %you% doing?", map), is(expected));
  }

  @Test
  public void testReplaceWithValuesEscaping() throws Exception {
    TemplateReplacer test = new TemplateReplacer("${", "}");
    String text = "${a} fine ${day} to ${die}";
    String expected = "never a fine ${day} to start living";
    Map<String,String> map = new HashMap<>();
    map.put("a", "never a");
    map.put("die", "start living");

    assertThat(test.replaceWithValues(text, map), is(expected));
  }

  @Test
  public void testReplaceWithValuesNull() throws Exception {
    TemplateReplacer test = new TemplateReplacer("${", "}");
    assertThat(test.replaceWithValues(null, null), is(nullValue()));
  }

  @Test
  public void testGetTemplateKeysWithCurlies() throws Exception {
    TemplateReplacer test = new TemplateReplacer("{", "}");
    assertThat(test.getTemplateKeys("Hello {my} world"), is(Collections.singleton("my")));
  }

  @Test
  public void testGetTemplateKeysWithEmptyEnd() throws Exception {
    TemplateReplacer test = new TemplateReplacer("$", "");
    assertThat(test.getTemplateKeys("$start Hello $my2nd world $end"), is((Set<String>) new HashSet<>(Arrays.asList("start", "my2nd", "end"))));
  }

}