package dk.lockfuglsang.jira.plugins.template.template;

import com.atlassian.jira.web.action.JiraWebActionSupport;

/**
 * Created by rlf on 11-08-14.
 */
public class PluginConfigWebworkModuleAction extends JiraWebActionSupport {
  private String pre;
  private String post;

  @Override
  public String doDefault() throws Exception {
    // TODO: rlf: 11 Aug 2014 - Load stuff
    if (pre == null) {
      pre = PluginSettings.getPre(getApplicationProperties());
    }
    if (post == null) {
      post = PluginSettings.getPost(getApplicationProperties());
    }
    return INPUT;
  }

  @Override
  protected String doExecute() throws Exception {
    // TODO: rlf: 11 Aug 2014 - Store stuff
    PluginSettings.setPre(getApplicationProperties(), pre);
    PluginSettings.setPost(getApplicationProperties(), post);
    return returnCompleteWithInlineRedirectAndMsg("/plugins/servlet/upm#manage", getText("template-rollout.config.saved"), MessageType.SUCCESS, false, "upm-messages");
  }

  public String getDescriptionHtml() {
    return "<div class='aui-message info'><span class='aui-icon icon-info'></span><p>"
      + getUntransformedRawText("template-rollout.config.description")
      + "</p></div>";
  }

  public String getPre() {
    return pre;
  }

  public void setPre(String pre) {
    this.pre = pre;
  }

  public String getPost() {
    return post;
  }

  public void setPost(String post) {
    this.post = post;
  }
}
