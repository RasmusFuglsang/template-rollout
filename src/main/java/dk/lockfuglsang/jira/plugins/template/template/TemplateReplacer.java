package dk.lockfuglsang.jira.plugins.template.template;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Simple template replacer supporting the replacement of patterns in strings.
 */
public class TemplateReplacer {

  private final Pattern pattern;

  public TemplateReplacer() {
    this("%", "%");
  }

  public TemplateReplacer(String pre, String post, String pattern) {
    this.pattern = Pattern.compile(Pattern.quote(pre) + "(" + pattern + ")" + Pattern.quote(post));
  }

  public TemplateReplacer(String pre, String post) {
    this(pre, post, "[a-zA-Z_\\-0-9]+");
  }

  public Set<String> getTemplateKeys(String data) {
    if (data == null) {
      return Collections.emptySet();
    }
    Matcher matcher = pattern.matcher(data);
    Set<String> keys = new HashSet<>();
    while (matcher.find()) {
      keys.add(matcher.group(1));
    }
    return keys;
  }

  public String replaceWithValues(String data, Map<String,String> values) {
    if (data == null) {
      return data;
    }
    StringBuffer sb = new StringBuffer();
    Matcher matcher = pattern.matcher(data);
    while (matcher.find()) {
      String key = matcher.group(1);
      String value = values.get(key);
      if (value != null) {
        matcher.appendReplacement(sb, Matcher.quoteReplacement(value));
      } else {
        matcher.appendReplacement(sb, Matcher.quoteReplacement(matcher.group(0)));
      }
    }
    matcher.appendTail(sb);
    return sb.toString();
  }
}
