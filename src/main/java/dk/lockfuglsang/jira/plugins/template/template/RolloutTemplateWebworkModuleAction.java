package dk.lockfuglsang.jira.plugins.template.template;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.IssueInputParameters;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.attachment.CreateAttachmentParamsBean;
import com.atlassian.jira.issue.customfields.OperationContextImpl;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.OrderableField;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.operation.IssueOperations;
import com.atlassian.jira.ofbiz.AbstractOfBizValueWrapper;
import com.atlassian.jira.plugin.webresource.JiraWebResourceManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.PathUtils;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.jira.web.util.AttachmentException;
import com.atlassian.upm.api.license.PluginLicenseManager;
import com.atlassian.upm.api.license.entity.PluginLicense;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The main action for rolling out a template
 */
public class RolloutTemplateWebworkModuleAction extends JiraWebActionSupport {
  private static final Logger log = LoggerFactory.getLogger(RolloutTemplateWebworkModuleAction.class);

  private final IssueService issueService;
  private final IssueManager issueManager;
  private final IssueFactory issueFactory;
  private final IssueLinkManager issueLinkManager;
  private final ProjectService projectService;
  private final JiraAuthenticationContext authenticationContext;
  private TemplateReplacer replacer;

  private Long id;
  private Long pid;
  private String baseUrl;
  private final JiraWebResourceManager webResourceManager;
  private final PluginLicenseManager licenseManager;
  private Map<String, Object> fieldValuesHolder = null;
  private List<Issue> issues;

  public RolloutTemplateWebworkModuleAction(IssueService issueService, IssueManager issueManager,
                                            IssueFactory issueFactory, IssueLinkManager issueLinkManager,
                                            ProjectService projectService, JiraAuthenticationContext authenticationContext,
                                            JiraWebResourceManager webResourceManager, PluginLicenseManager licenseManager) {
    this.issueService = issueService;
    this.issueManager = issueManager;
    this.issueFactory = issueFactory;
    this.issueLinkManager = issueLinkManager;
    this.projectService = projectService;
    this.authenticationContext = authenticationContext;
    this.webResourceManager = webResourceManager;
    this.licenseManager = licenseManager;
    replacer = new TemplateReplacer(PluginSettings.getPre(getApplicationProperties()), PluginSettings.getPost(getApplicationProperties()));
  }

  /**
   * The validation logic of you action.  Gets called before doExecute().
   * Use addError() to add form errors for specific fields.
   * Use addErrorMessage() for adding generic form errors
   */
  protected void doValidation() {
    includeResources();
    validateLicense();
    // Validation logic
    // TODO: rlf: 06 Aug 2014 - Validate that all "template-fields" denote valid fields.
  }

  private void validateLicense() {
    if (getApplicationProperties().getOption("dk.lockfuglsang.jira.plugins.template:useLicense")) {
      if (licenseManager.getLicense().isDefined()) {
        PluginLicense pluginLicense = licenseManager.getLicense().get();
        if (pluginLicense.getError().isDefined()) {
          addErrorMessage(pluginLicense.getError().get().toString());
        }
      } else {
        addErrorMessage(getText("template-rollout.error.nolicense"));
      }
    }
  }

  /**
   * The business logic of your form.
   * Only gets called if validation passes.
   *
   * @return the view to display - should usually be "success"
   */
  @RequiresXsrfCheck
  protected String doExecute() throws Exception {
    // Business Logic
    long pid = Long.parseLong(getHttpRequest().getParameter("pid"), 10);
    ApplicationUser appUser = authenticationContext.getLoggedInUser();
    Project project = projectService.getProjectById(appUser, pid).getProject();
    List<OrderableField> templateFields = getTemplateFields();
    // TODO: rlf: 06 Aug 2014 - Support use of "multi-selection", i.e. choose multiple labels to assign to all.
    List<Map<Field, Object>> permutationMap = createPermutations(templateFields);
    List<Issue> newIssues = new ArrayList<>();
    for (Map<Field, Object> valueMap : permutationMap) {
      Issue issue = getIssue();
      Map<Long, Issue> old2newConversion = new HashMap<>();
      // "Pure" cloning
      Issue newIssue = cloneIssue(appUser, issue, project, valueMap, null, old2newConversion);
      cloneLinks(appUser, issue, old2newConversion);
      // Setting the values
      newIssue = updateValues(appUser, newIssue, valueMap, null);
      newIssues.add(newIssue);
    }
    if (!hasAnyErrors()) {
      this.issues = newIssues;
      if (newIssues.size() == 1) {
        // Forward to the newly created one
        return returnCompleteWithInlineRedirect("/browse/" + newIssues.get(0).getKey());
      } else if (newIssues.size() > 1) {
        return returnCompleteWithInlineRedirect("/browse/" + newIssues.get(0).getKey() + "?jql=id%20in%20" + toInString(newIssues));
      } else {
        // Go back to the original
        return returnComplete("/browse/" + getIssue().getKey());
      }
    } else {
      return ERROR;
    }
  }

  private String toInString(List<Issue> newIssues) {
    return Arrays.toString(newIssues.toArray())
      .replaceAll("\\[", "(")
      .replaceAll("\\]", ")")
      .replaceAll(" ", "");
  }

  private Issue updateValues(ApplicationUser user, Issue issue, Map<Field, Object> valueMap, Issue parent) {
    IssueInputParameters params = issueService.newIssueInputParameters();
    IssueService.UpdateValidationResult validationResult = issueService.validateUpdate(user, issue.getId(), params);
    params.setFieldValuesHolder(validationResult.getFieldValuesHolder());
    // This stores the values
    setValues(params, valueMap);
    validationResult = issueService.validateUpdate(user, issue.getId(), params);
    if (validationResult.isValid()) {
      IssueService.IssueResult update = issueService.update(user, validationResult);
      if (update.isValid()) {
        issue = update.getIssue();
      } else {
        addErrorMessages(update, issue);
      }
    } else {
      addErrorMessages(validationResult, issue);
    }
    for (Issue subTask : issue.getSubTaskObjects()) {
      updateValues(user, subTask, valueMap, issue);
    }
    return issue;
  }

  private void cloneLinks(ApplicationUser user, Issue originalIssue, Map<Long, Issue> old2newConversion) throws CreateException {
    // Links
    List<IssueLink> links = new ArrayList<>();
    links.addAll(issueLinkManager.getInwardLinks(originalIssue.getId()));
    links.addAll(issueLinkManager.getOutwardLinks(originalIssue.getId()));
    for (IssueLink oldLink : links) {
      Long src = old2newConversion.get(oldLink.getSourceId()) != null ? old2newConversion.get(oldLink.getSourceId()).getId() : oldLink.getSourceId();
      Long dst = old2newConversion.get(oldLink.getDestinationId()) != null ? old2newConversion.get(oldLink.getDestinationId()).getId() : oldLink.getDestinationId();
      Long type = oldLink.getLinkTypeId();
      if (issueLinkManager.getIssueLink(src, dst, type) == null) {
        issueLinkManager.createIssueLink(src, dst, type, oldLink.getSequence(), user);
      }
    }
    for (Issue subTask : originalIssue.getSubTaskObjects()) {
      cloneLinks(user, subTask, old2newConversion);
    }
  }

  private Issue cloneIssue(ApplicationUser user, Issue issue, Project targetProject, Map<Field, Object> valueMap, Issue parent, Map<Long, Issue> old2newConversion) throws CreateException, AttachmentException {
    MutableIssue cloneIssue = issueFactory.cloneIssue(issue);
    old2newConversion.put(issue.getId(), cloneIssue);

    cloneIssue.setProjectObject(targetProject);
    if (parent != null) {
      cloneIssue.setParentObject(parent);
    }
    Map<String, String> stringValueMap = toStringValueMap(valueMap);
    cloneIssue.setSummary(replacer.replaceWithValues(cloneIssue.getSummary(), stringValueMap));
    cloneIssue.setDescription(replacer.replaceWithValues(cloneIssue.getDescription(), stringValueMap));

    // This makes the clone
    Issue clone = issueManager.createIssueObject(user, cloneIssue);
    old2newConversion.put(issue.getId(), clone);

    // Attachments
    for (Attachment att : issue.getAttachments()) {
      // TODO: rlf: 07 Aug 2014 - Perhaps just make issue-links to the original issue?
      String path = getAttachmentPath(issue, att, parent != null ? parent.getIssueType().getId() : null);
      CreateAttachmentParamsBean attachmentBean = new CreateAttachmentParamsBean.Builder()
        .issue(clone)
        .author(att.getAuthorObject())
        .contentType(att.getMimetype())
        .createdTime(att.getCreated())
        .zip(att.isZip())
        .thumbnailable(att.isThumbnailable())
        .filename(att.getFilename())
        .copySourceFile(true)
        .file(new File(path))
        .build();
      ComponentAccessor.getAttachmentManager().createAttachment(attachmentBean);
    }

    // Sub tasks
    for (Issue subTask : issue.getSubTaskObjects()) {
      cloneIssue(user, subTask, targetProject, valueMap, clone, old2newConversion);
    }
    return clone;
  }

  private String getAttachmentPath(Issue issue, Attachment att, String parentType) {
    String path = PathUtils.joinPaths(ComponentAccessor.getAttachmentPathManager().getAttachmentPath(),
      issue.getProjectObject().getOriginalKey(), parentType,  issue.getProjectObject().getOriginalKey() + "-" + issue.getNumber(), att.getId().toString());
    if (parentType != null && Files.exists(Paths.get(path))) {
      return path;
    }
    path = PathUtils.joinPaths(ComponentAccessor.getAttachmentPathManager().getAttachmentPath(),
      issue.getProjectObject().getOriginalKey(), issue.getProjectObject().getOriginalKey() + "-" + issue.getNumber(), att.getId().toString());
    return path;
  }

  /**
   * Flattens the valueMap to include only the "dumb" values used for replacing in summary and description
   *
   * @param valueMap
   * @return
   */
  private Map<String, String> toStringValueMap(Map<Field, Object> valueMap) {
    Map<String, String> map = new HashMap<>();
    for (Map.Entry<Field, Object> entry : valueMap.entrySet()) {
      String stringValue = asStringValue(entry.getValue());
      if (stringValue != null) {
        map.put(entry.getKey().getId(), stringValue);
        map.put(entry.getKey().getName(), stringValue);
      }
    }
    return map;
  }

  private String asStringValue(Object value) {
    String stringValue = null;
    if (value instanceof Option) {
      stringValue = ((Option) value).getValue();
    } else if (value != null) {
      stringValue = "" + value;
    }
    return stringValue;
  }

  private String asStringID(Object value) {
    String stringValue = null;
    if (value instanceof Option) {
      stringValue = Long.toString(((Option) value).getOptionId(), 10);
    } else if (value instanceof AbstractOfBizValueWrapper) {
      stringValue = ((AbstractOfBizValueWrapper)value).getString("id");
    } else if (value != null) {
      stringValue = "" + value;
    }
    return stringValue;
  }

  private void addErrorMessages(IssueService.IssueValidationResult validationResult, Issue clone) {
    // Handle field-specific errors...
    Map<String, String> errors = validationResult.getErrorCollection().getErrors();
    for (String key : errors.keySet()) {
      addErrorMessage(getI18nHelper().getText("template-rollout.error.field.error", clone.getKey(), errors.get(key)));
    }
    addErrorMessages(validationResult.getErrorCollection().getErrorMessages());
  }

  private void setValues(IssueInputParameters params, Map<Field, Object> valueMap) {
    for (Field field : valueMap.keySet()) {
      Object value = valueMap.get(field);
      String stringValue = asStringID(value);
      params.getActionParameters().put(field.getId(), new String[]{stringValue});
    }
  }

  /**
   * Creates all the permutations of the chosen fields.
   */
  private List<Map<Field, Object>> createPermutations(List<OrderableField> templateFields) {
    List<Map<Field, Object>> map = new LinkedList<>();
    addPermutations(map, templateFields, 0, new HashMap<Field, Object>());
    return map;
  }

  private void addPermutations(List<Map<Field, Object>> mapList, List<OrderableField> fields, int index, Map<Field, Object> currentMap) {
    if (index < fields.size()) {
      OrderableField field = fields.get(index);
      String key = field.getId();
      String[] values = getHttpRequest().getParameterValues(key);
      if (values != null) {
        for (String valueKey : values) {
          Map<Field, Object> copy = new HashMap<>(currentMap);
          Object value = getValue(field, key, valueKey);
          if (value != null) {
            copy.put(field, value);
          }
          addPermutations(mapList, fields, index + 1, copy);
        }
      } else {
        // Simply skip this one...
        addPermutations(mapList, fields, index + 1, currentMap);
      }
    } else {
      mapList.add(currentMap);
    }
  }

  private Object getValue(OrderableField field, String key, String valueKey) {
    Object value = null;
    if (field instanceof CustomField) {
      try {
        Option option = ComponentAccessor.getOptionsManager().findByOptionId(Long.parseLong(valueKey));
        if (option != null && option.getRelatedCustomField().getFieldId().equals(field.getId())) {
          value = option;
        }
      } catch (NumberFormatException ignored) {
        // Just try something else
      }
    }
    if (value == null && asLong(valueKey) != null) {
      Map<String,Object> valueMap = new HashMap<>();
      valueMap.put(key, Arrays.asList(asLong(valueKey)));
      value = field.getValueFromParams(valueMap);
    }
    if (value == null) {
      Map<String, Object> valueMap = new HashMap<>();
      valueMap.put(key, Arrays.asList(valueKey));
      value = field.getValueFromParams(valueMap);
    }
    if (value instanceof Collection) {
      return ((Collection)value).iterator().next();
    }
    return value;
  }

  private Long asLong(String valueKey) {
    try {
      return Long.parseLong(valueKey, 10);
    } catch (NumberFormatException e) {
      // Ignored
    }
    return null;
  }

  /**
   * The initialization logic of the form.
   * Validation does NOT happen before this.
   *
   * @return the view to display - should usually be "input"
   */
  public String doDefault() throws Exception {
    baseUrl = getHttpRequest().getContextPath();
    final Issue issue = getIssueObject();
    if (issue == null) {
      return ERROR;
    }

    includeResources();
    validateLicense();
    if (hasAnyErrors()) {
      return ERROR;
    }

    // populate the values holder from the from the issue (it's initial value)
    //getFixForField().populateFromIssue(getFieldValuesHolder(), issue);

    // Check to see if we can update the issue
    final IssueInputParameters issueInputParameters = issueService.newIssueInputParameters();

    //issueInputParameters.setFixVersionIds(null);

    // This should validate whether the user is able to edit the issue
    IssueService.UpdateValidationResult localResult = issueService.validateUpdate(authenticationContext.getLoggedInUser(), getId(), issueInputParameters);
    if (!localResult.isValid()) {
      this.addErrorCollection(localResult.getErrorCollection());
      return ERROR;
    }

    // Initialization logic
    return INPUT;
  }

  public String doVariables() throws Exception {
    HttpServletRequest req = getHttpRequest();
    baseUrl = req.getContextPath();
    String pidField = req.getParameter("pid-field");
    if (pidField == null || "null".equals(pidField)) {
      pidField = req.getParameter("pid");
    }
    if (pidField == null || "null".equals(pidField)) {
      log.info("Unable to locate pid in " + req.getParameterMap());
    }
    log.info("pidField: " + pidField);
    log.info("Params: " + req.getParameterMap());
    pid = Long.parseLong(pidField);
    return "variables";
  }

  @SuppressWarnings("UnusedDeclaration")
  public String getProjectsJSON() {
    StringBuilder sb = new StringBuilder();
    Set<IssueType> issueTypes = new HashSet<>();
    addIssueTypes(issueTypes, getIssueObject());
    String opt = "\"label\":\"{1}\",\"value\":\"{0,number,#}\",\"icon\":\"{3}\",\"selected\":{2}";
    long pid = getIssueObject().getProjectId();
    ServiceOutcome<List<Project>> allProjects = projectService.getAllProjects(authenticationContext.getLoggedInUser());
    sb.append("[{\"label\":\"Projects\",\"items\":[");
    boolean isFirst = true;
    if (allProjects.isValid() && !allProjects.getErrorCollection().hasAnyErrors()) {
      for (Project project : allProjects.get()) {
        if (project.getIssueTypes().containsAll(issueTypes)) {
          URI avatar = ComponentAccessor.getAvatarService().getProjectAvatarURL(project, Avatar.Size.SMALL);
          if (!isFirst) {
            sb.append(",");
          }
          sb.append("{" + MessageFormat.format(opt, project.getId(), project.getName(), project.getId() == pid ? "true" : "false", avatar.toString()) + "}");
          isFirst = false;
        } else {
          log.info("Project " + project.getName() + " doesn't have all the issue-types required for template " + getIssueObject().getKey());
        }
      }
    }
    sb.append("]}]");
    return sb.toString();
  }

  private void addIssueTypes(Set<IssueType> issueTypes, Issue issueObject) {
    issueTypes.add(issueObject.getIssueType());
    for (Issue subTask : issueObject.getSubTaskObjects()) {
      addIssueTypes(issueTypes, subTask);
    }
  }

  @SuppressWarnings("UnusedDeclaration")
  public String getTemplateEditHtml() {
    final OperationContextImpl operationContext = new OperationContextImpl(IssueOperations.EDIT_ISSUE_OPERATION, getFieldValuesHolder());
    Issue issueObject = getIssueObject();
    MutableIssue issue = issueFactory.cloneIssue(issueObject);
    issue.setProjectId(pid);
    StringBuilder sb = new StringBuilder();
    for (OrderableField field : getTemplateFields()) {
      sb.append("<div class='qf-field'>");
      sb.append(field.getEditHtml(null, operationContext, this, issue, getDisplayParams()));
      sb.append("</div>\n");
    }
    return sb.toString();
  }

  public List<OrderableField> getTemplateFields() {
    Issue issueObject = getIssueObject();
    return new ArrayList<>(getTemplateFields(issueObject));
  }

  private Set<OrderableField> getTemplateFields(Issue issueObject) {
    Set<OrderableField> fields = new LinkedHashSet<>();
    String summary = issueObject.getSummary() + " " + issueObject.getDescription();
    for (String key : replacer.getTemplateKeys(summary)) {
      Field field = getField(key);
      if (field == null) {
        field = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName(key);
      }
      if (field instanceof OrderableField) {
        fields.add((OrderableField) field);
      } else {
        log.warn("Unable to locate a valid field for key '" + key + "'");
      }
    }
    for (Issue subTask : issueObject.getSubTaskObjects()) {
      fields.addAll(getTemplateFields(subTask));
    }
    return fields;
  }

  private Map<String, Object> getDisplayParams() {
    // This will render the field in it's "aui" state.
    final Map<String, Object> displayParams = new HashMap<>();
    displayParams.put("theme", "aui");
    return displayParams;
  }

  private Map<String, Object> getFieldValuesHolder() {
    if (fieldValuesHolder == null) {
      fieldValuesHolder = new HashMap<>();
    }
    return fieldValuesHolder;
  }

  private void includeResources() {
    webResourceManager.requireResource("jira.webresources:jira-fields");
    webResourceManager.requireResource("dk.logiva.jira.plugins.template-rollout:template-rollout-resources");
  }

  /**
   * Used by the decorator
   */
  @SuppressWarnings("UnusedDeclaration")
  public Project getProject() {
    return getIssue().getProjectObject();
  }

  /**
   * Used by the decorator
   */
  public Issue getIssue() {
    return getIssueObject();
  }

  public Issue getIssueObject() {
    final IssueService.IssueResult issueResult = issueService.getIssue(authenticationContext.getLoggedInUser(), id);
    if (!issueResult.isValid()) {
      this.addErrorCollection(issueResult.getErrorCollection());
      return null;
    }

    return issueResult.getIssue();
  }

  // Getter and Setters for passing the form params

  public Long getId() {
    return id;
  }

  @SuppressWarnings("UnusedDeclaration")
  public void setId(Long id) {
    this.id = id;
  }

  public boolean getHasErrors() {
    return super.hasAnyErrors();
  }

  public Long getPid() {
    return pid;
  }

  public void setPid(Long pid) {
    this.pid = pid;
  }

  public List<Issue> getIssues() {
    return issues;
  }

  public String getBaseUrl() {
    return baseUrl;
  }
}
