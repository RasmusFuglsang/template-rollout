package dk.lockfuglsang.jira.plugins.template.template;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugin.webfragment.conditions.AbstractIssueWebCondition;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;

/**
 * Only show the action, if the issue in question is a template-issue.
 */
public class IsTemplateIssue extends AbstractIssueWebCondition {
  private final ApplicationProperties props;
  private final TemplateReplacer replacer;

  public IsTemplateIssue(ApplicationProperties props) {
    this.props = props;
    replacer = new TemplateReplacer(PluginSettings.getPre(props), PluginSettings.getPost(props));
  }

  @Override
  public boolean shouldDisplay(ApplicationUser applicationUser, Issue issue, JiraHelper jiraHelper) {
    // TODO: rlf: 07 Aug 2014 - Perhaps also check that the pattern actually denotes something valid?
    return hasTemplatePattern(issue);
  }

  private boolean hasTemplatePattern(Issue issue) {
    if (issue == null || issue.isSubTask()) {
      return false;
    }
    String testString = issue.getSummary() + " " + issue.getDescription();
    if (!replacer.getTemplateKeys(testString).isEmpty()) {
      return true;
    }
    for (Issue subTask : issue.getSubTaskObjects()) {
      if (hasTemplatePattern(subTask)) {
        return true;
      }
    }
    return false;
  }
}
