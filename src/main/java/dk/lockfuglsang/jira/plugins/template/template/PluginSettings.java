package dk.lockfuglsang.jira.plugins.template.template;

import com.atlassian.jira.config.properties.ApplicationProperties;

/**
 * Created by rlf on 11-08-14.
 */
public enum PluginSettings {;
  private static String PRE = "dk.logiva.jira.plugins:pre";
  private static String POST = "dk.logiva.jira.plugins:post";

  public static String getPre(ApplicationProperties props) {
    String pre = props.getDefaultBackedString(PRE);
    if (pre == null) {
      pre = "%";
    }
    return pre;
  }

  public static void setPre(ApplicationProperties props, String pre) {
    props.setString(PRE, pre);
  }
  public static String getPost(ApplicationProperties props) {
    String value = props.getDefaultBackedString(POST);
    if (value == null) {
      value = "%";
    }
    return value;
  }

  public static void setPost(ApplicationProperties props, String value) {
    props.setString(POST, value);
  }

}
