AJS.$(function () {
    // Function for getting the issue key of the issue being edited.
    var getIssueKey = function(){
        if (JIRA.IssueNavigator.isNavigator()){
            return JIRA.IssueNavigator.getSelectedIssueKey();
        } else {
            return AJS.$.trim(AJS.$("#key-val").text());
        }
    };

    // Function for getting the project key of the Issue being edited.
    var getProjectKey = function(){
        var issueKey = getIssueKey();
        if (issueKey){
            return issueKey.match("[A-Z]*")[0];
        }
    };

    JIRA.Dialogs.templaterollout = new JIRA.FormDialog({
        id: "template-dialog",
        trigger: "a.issueaction-template-rollout",
        ajaxOptions: JIRA.Dialogs.getDefaultAjaxOptions,
        onSuccessfulSubmit : function() {
            // This method is used to define behaviour on a successful form submission.
            // We want to get the versions specified then place them in the view.
            // TODO: Do something here
        },
        onDialogFinished : function() {  // This function is used to define behaviour after the form has finished
            // We want to display a notification telling people that the fix version has been updated.
            // If it is displaying in the Issue Navigator we want to also show the issue key of the issue updated.
            if (JIRA.IssueNavigator.isNavigator()){
                JIRA.Messages.showSuccessMsg(AJS.I18n.getText("template-rollout.success.issue", getIssueKey()));
            } else {
                JIRA.Messages.showSuccessMsg(AJS.I18n.getText("template-rollout.success"));
            }
        },
        autoClose : true
    });

});